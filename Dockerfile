FROM node:carbon

# Create app directory
WORKDIR /usr/src/store

# Bundle app source
COPY . .

# Gives execute permission for store.js
CMD [ "chmod", "+x store.js" ]