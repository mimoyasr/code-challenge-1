#!/usr/local/bin/node

const DATA = "data.json";
const fs = require('fs');
let params = process.argv;
let command = params[2];
let store = new Store(DATA);

switch (command) {
    case "add":
        if (params[3] && params[4]) {
            store.add(params[3], params[4]);
        } else {
            console.error(`parameter is missing`);
        }
        break;
    case "list":
        store.list();
        break;
    case "get":
        if (params[3]) {
            store.get(params[3]);
        } else {
            console.error(`parameter is missing`);
        }
        break;
    case "remove":
        if (params[3]) {
            store.remove(params[3]);
        } else {
            console.error(`parameter is missing`);
        }
        break;
    case "clear":
        store.clear();
        break;
    default:
        console.error(`command ${command} dosen\`t match any known command`);
}
store.save();

function Store(filename) {
    try {
        this.data = JSON.parse(fs.readFileSync(filename));
    } catch (err) {
        throw new Error(`Error while reading data.json,
         maybe the file is missing or it dosen\`t have the permission`);
    }

    this.add = function(key, val) {
        this.data[key] = val;
        console.info(`${key} added`);
    }
    this.list = function() {
        for (let key in this.data) {
            if (this.data.hasOwnProperty(key)) {
                console.log(`${key} : ${this.data[key]}`);
            }
        }
    }
    this.get = function(key) {
        if (this.data.hasOwnProperty(key)) {
            console.info(`${key} : ${this.data[key]}`);
        } else {
            console.error(`Key ${key} not found!`)
        }
    }
    this.remove = function(key) {
        if (this.data.hasOwnProperty(key)) {
            delete this.data[key];
            console.info(`${key} removed`);
        } else {
            console.error(`Key ${key} not found!`)
        }
    }
    this.clear = function() {
        this.data = {};
        console.info(`cleared`);
    }
    this.save = function() {
        fs.writeFileSync('data.json', JSON.stringify(this.data));
    }
}